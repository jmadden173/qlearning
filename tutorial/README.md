# Tutorial Code

## cartpole_qlearning.py

Tests basic Q learning against the Open AI gym enviroment cartpole.
See [pythonprogramming.nets](https://pythonprogramming.net/q-learning-reinforcement-learning-python-tutorial/) for more information.

## blob_dqn.py

Reinforcement learning based on a created enviroment where the AI has to find its way to the exit while avoiding enemies. This is the follow up to cartpole_qlearning.py.
See [pythonprogramming.nets](https://pythonprogramming.net/deep-q-learning-dqn-reinforcement-learning-python-tutorial/) for more information.

## cnn.py

A basic Convolutional Neural Network taken from the tensorflow docs. Uses the cifar10 dataset to classify images into 10 categories.
See [tensorflow.org](https://www.tensorflow.org/tutorials/images/cnn) for more information.
