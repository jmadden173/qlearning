import gym

# general
import os
import numpy as np
import pickle
import gym
import time
from tqdm import tqdm
import random
from collections import deque
from PIL import Image
import cv2

# keras
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout, Conv2D,  MaxPooling2D, Activation, Flatten
from keras.callbacks import TensorBoard
from keras.optimizers import Adam


# CONSTANTS
DISCOUNT = 0.99
REPLAY_MEMORY_SIZE = 50_000
MIN_REPLAY_MEMORY_SIZE = 1000
MODEL_NAME = "256x2"
MINIBATCH_SIZE = 64
UPDATE_TARGET_EVERY = 5
MIN_REWARD = -200

# env settings
EPISODES = 20_000

# exploration settings
epsilon = 1
EPSILON_DECAY = 0.99975
MIN_EPSILON = 0.001

# stats
AGGREGATE_STATS_EVERY = 50
SHOW_PREVIEW = True


print(tf.__version__)
# disable gpu bc to much memory is being used
tf.config.set_visible_devices([], 'GPU')
#gpus = tf.config.experimental.list_physical_devices('GPU')
#if gpus:
#    try:
#        for gpu in gpus:
#            tf.config.experimental.set_memory_growth(gpu, True)
#        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
#    except RuntimeError as e:
#        print(e)




# Blob enviroment
class Blob:
    def __init__(self, size):
        self.size = size
        self.x = np.random.randint(0, size)
        self.y = np.random.randint(0, size)

    def __str__(self):
        return f"Blob ({self.x}, {self.y})"

    def __sub__(self, other):
        return (self.x-other.x, self.y-other.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def action(self, choice):
        '''
        Gives us 9 total movement options. (0,1,2,3,4,5,6,7,8)
        '''
        if choice == 0:
            self.move(x=1, y=1)
        elif choice == 1:
            self.move(x=-1, y=-1)
        elif choice == 2:
            self.move(x=-1, y=1)
        elif choice == 3:
            self.move(x=1, y=-1)

        elif choice == 4:
            self.move(x=1, y=0)
        elif choice == 5:
            self.move(x=-1, y=0)

        elif choice == 6:
            self.move(x=0, y=1)
        elif choice == 7:
            self.move(x=0, y=-1)

        elif choice == 8:
            self.move(x=0, y=0)

    def move(self, x=False, y=False):

        # If no value for x, move randomly
        if not x:
            self.x += np.random.randint(-1, 2)
        else:
            self.x += x

        # If no value for y, move randomly
        if not y:
            self.y += np.random.randint(-1, 2)
        else:
            self.y += y

        # If we are out of bounds, fix!
        if self.x < 0:
            self.x = 0
        elif self.x > self.size-1:
            self.x = self.size-1
        if self.y < 0:
            self.y = 0
        elif self.y > self.size-1:
            self.y = self.size-1


class BlobEnv:
    SIZE = 10
    RETURN_IMAGES = True
    MOVE_PENALTY = 1
    ENEMY_PENALTY = 300
    FOOD_REWARD = 25
    OBSERVATION_SPACE_VALUES = (SIZE, SIZE, 3)  # 4
    ACTION_SPACE_SIZE = 9
    PLAYER_N = 1  # player key in dict
    FOOD_N = 2  # food key in dict
    ENEMY_N = 3  # enemy key in dict
    # the dict! (colors)
    d = {1: (255, 175, 0),
         2: (0, 255, 0),
         3: (0, 0, 255)}

    def reset(self):
        self.player = Blob(self.SIZE)
        self.food = Blob(self.SIZE)
        while self.food == self.player:
            self.food = Blob(self.SIZE)
        self.enemy = Blob(self.SIZE)
        while self.enemy == self.player or self.enemy == self.food:
            self.enemy = Blob(self.SIZE)

        self.episode_step = 0

        if self.RETURN_IMAGES:
            observation = np.array(self.get_image())
        else:
            observation = (self.player-self.food) + (self.player-self.enemy)
        return observation

    def step(self, action):
        self.episode_step += 1
        self.player.action(action)

        #### MAYBE ###
        #enemy.move()
        #food.move()
        ##############

        if self.RETURN_IMAGES:
            new_observation = np.array(self.get_image())
        else:
            new_observation = (self.player-self.food) + (self.player-self.enemy)

        if self.player == self.enemy:
            reward = -self.ENEMY_PENALTY
        elif self.player == self.food:
            reward = self.FOOD_REWARD
        else:
            reward = -self.MOVE_PENALTY

        done = False
        if reward == self.FOOD_REWARD or reward == -self.ENEMY_PENALTY or self.episode_step >= 200:
            done = True

        return new_observation, reward, done

    def render(self):
        img = self.get_image()
        img = img.resize((300, 300))  # resizing so we can see our agent in all its glory.
        cv2.imshow("image", np.array(img))  # show it!
        cv2.waitKey(1)

    # FOR CNN #
    def get_image(self):
        env = np.zeros((self.SIZE, self.SIZE, 3), dtype=np.uint8)  # starts an rbg of our size
        env[self.food.x][self.food.y] = self.d[self.FOOD_N]  # sets the food location tile to green color
        env[self.enemy.x][self.enemy.y] = self.d[self.ENEMY_N]  # sets the enemy location to red
        env[self.player.x][self.player.y] = self.d[self.PLAYER_N]  # sets the player tile to blue
        img = Image.fromarray(env, 'RGB')  # reading to rgb. Apparently. Even tho color definitions are bgr. ???
        return img



# init env
env = BlobEnv()

# for stats
ep_rewards = [-200]



# takes from here for update to tensorflow 2.0
# https://stackoverflow.com/questions/58711624/modifying-tensorboard-in-tensorflow-2-0
class ModifiedTensorBoard(TensorBoard):
    # overriding init to set initial step and writer
    # we want one log file for all .fit() calls
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step = 1
        self.writer = tf.summary.create_file_writer(self.log_dir)
        self._log_write_dir = os.path.join(self.log_dir, MODEL_NAME)


    # overrided, to prevent creation of default log writer
    def set_model(self, model):
        pass

    # overrided, saves logs with step number
    # (normally .fit() will begin writing from the 0th step)
    def on_epoch_end(self, epoch, logs=None):
        self.update_stats(**logs)

    # overrided no need to save anything at epoch end
    def on_batch_end(self, batch, logs=None):
        pass

    # overrided so writer is not closed
    def on_train_end(self, _):
        pass

    # writer for saving custom metrics
    def update_stats(self, **stats):
        self._write_logs(stats, self.step)

    # writes logs to file
    def _write_logs(self, logs, index):
        with self.writer.as_default():
            for name, value in logs.items():
                tf.summary.scalar(name, value, step=index)
                self.step += 1
                self.writer.flush()



class DQNAgent:
    def __init__(self):
        # main model
        self.model = self.create_model()

        # target network
        self.target_model = self.create_model()
        self.target_model.set_weights(self.model.get_weights())

        # replay buffer (numpy array) to store that contains last n steps for
        # training
        self.replay_memory = deque(maxlen=REPLAY_MEMORY_SIZE)

        # custom tensorboard object
        #self.tensorboard = TensorBoard(
        #    log_dir="logs/{}-{}".format(MODEL_NAME, int(time.time()))
        #)
        self.tensorboard = ModifiedTensorBoard(
            log_dir="logs/{}-{}".format(MODEL_NAME, int(time.time()))
        )

        # used to decide when to update target network with
        # main network's weights
        self.target_update_counter = 0


    def create_model(self):
        '''create a convolutional neural network'''

        model = Sequential()

        # input 
        model.add(Conv2D(256, (3,3), input_shape=env.OBSERVATION_SPACE_VALUES))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.2))

        model.add(Conv2D(256, (3,3)))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.2))

        model.add(Flatten())
        model.add(Dense(64))

        # output
        model.add(Dense(env.ACTION_SPACE_SIZE, activation="linear"))
        model.compile(loss="mse", optimizer=Adam(lr=0.001), metrics=["accuracy"])

        return model


    def update_replay_memory(self, transition):
        '''adds step data to memory replay array

        :param transition: (observation_space, action, reward, new_observation, done)
        '''

        self.replay_memory.append(transition)


    def get_qs(self, state):
        '''queries main network for Q value for a given state
        
        :param state: the enviroment state
        '''
        return self.model.predict(np.array(state).reshape(-1, *state.shape)/255)[0]


    def train(self, terminal_state, step):
        '''trains the neural network every step during a episode'''

        # start training if above a certain amount of samples
        if len(self.replay_memory) < MIN_REPLAY_MEMORY_SIZE:
            return

        # get a sample from the replay_memory to use in training
        minibatch = random.sample(self.replay_memory, MINIBATCH_SIZE)

        # NOTE i think were dividing by 255 to normalize the rgb
        # values from the env

        # gets the states from the minibatch, then query NN for q values
        current_states = np.array([transition[0] for transition in minibatch])/255
        current_qs_list = self.model.predict(current_states)
        
        # gets the futurestates from the minibatch, then query NN for q values
        # when using target network, query it, otherwise use main network ?????
        new_current_states = np.array([transition[3] for transition in minibatch])/255
        future_qs_list = self.target_model.predict(new_current_states)


        x = []
        y = []

        # enumerate batches
        for index, (current_state, action, reward, new_current_state, done) in enumerate(minibatch):
            # if not in terminal state aka if the env is done, get new Q
            # from future states, otherwise set to zero
            # almost like Q learning but only part of the equation is used
            if not done:
                max_future_q = np.max(future_qs_list[index])
                new_q = reward + DISCOUNT * max_future_q
            else:
                new_q = reward

            # update Q value for a given state
            current_qs = current_qs_list[index]
            current_qs[action] = new_q

            # append to training data
            x.append(current_state)
            y.append(current_qs)

        # fit on all samples as a batch
        # only have tensorboard callback if in terminal state
        self.model.fit(
            np.array(x)/255,
            np.array(y),
            batch_size=MINIBATCH_SIZE,
            verbose=0,
            shuffle=False,
            callbacks=[self.tensorboard] if terminal_state else None
            #callbacks=[self.tensorboard]
        )

        # update the counter every episode
        if terminal_state:
            self.target_update_counter += 1

        # if counter reaches desired values, update weights
        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0



agent = DQNAgent()
print(agent.model.summary())


# create models folder
if not os.path.isdir('models'):
    os.makedirs('models')


# loop for running episodes
for episode in tqdm(range(1, EPISODES + 1), ascii=True, unit="episodes"):

    # update tensorboard step to match episode
    #agent.tensorboard.step = episode

    # restart episode
    episode_reward = 0
    step = 1

    # reset env
    current_state = env.reset()

    # loop through until done
    done = False
    while not done:

        if np.random.random_sample() > epsilon:
            # get action from Q table
            action = np.argmax(agent.get_qs(current_state))

        else:
            # TODO change to sample for random action
            # get random state
            action = np.random.randint(0, env.ACTION_SPACE_SIZE)

        # step env
        new_state, reward, done = env.step(action)

        # count reward
        episode_reward += reward

        # display snapshot training process
        if (SHOW_PREVIEW) and (episode % AGGREGATE_STATS_EVERY == 0):
            env.render()

        # update replay buffer and train network
        agent.update_replay_memory((
            current_state,
            action,
            reward,
            new_state,
            done
        ))
        agent.train(done, step)

        # update state
        current_state = new_state
        step += 1


    # logs stats
    ep_rewards.append(episode_reward)
    if (episode % AGGREGATE_STATS_EVERY == 0) or (episode == 1):
        average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
        min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
        max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
        agent.tensorboard.update_stats(
            reward_avg=average_reward,
            reward_min=min_reward,
            reward_max=max_reward,
            epsilon=epsilon,
        )

        # Save model
        if min_reward >= MIN_REWARD:
            agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')


    # decay epsilon
    if epsilon > MIN_EPSILON:
        # exponential decay
        epsilon *= EPSILON_DECAY
        epislon = max(MIN_EPSILON, epsilon)