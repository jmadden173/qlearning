import gym
import numpy as np
import matplotlib.pyplot as plt


## CONSTANTS

# learning settings
LEARNING_RATE = 0.1
DISCOUNT = 0.95
EPISODES = 100000

# exploration settings
epsilon = 1
START_EPSILON_DECAYING = 1
END_EPSILON_DECAYING = EPISODES // 2
EPSILON_DECAY_VALUE = epsilon / (END_EPSILON_DECAYING - START_EPSILON_DECAYING)

# stat settings
SHOW_EVERY = 1000
STATS_EVERY = 100
SAVE_EVERY = 10


## INIT

# init env
env = gym.make("MountainCar-v0")

# where stats are store
ep_rewards = []
aggr_ep_rewards = {
    'episode': [],
    'avg': [],
    'max': [],
    'min': [],
}


# group observation space continues values to
# descrete values
DISCRETE_OS_SIZE = [40] * len(env.observation_space.high)
discrete_os_win_size = (env.observation_space.high - env.observation_space.low)/DISCRETE_OS_SIZE

print(discrete_os_win_size)

# create Q table
q_table = np.random.uniform(
    low=-2,
    high=0,
    size=(DISCRETE_OS_SIZE + [env.action_space.n])
)



def get_discrete_state(state):
    '''converts from env continuous state to a discrete state
    
    :param state: state from gym env
    '''

    discrete_state = (state - env.observation_space.low)/discrete_os_win_size
    return tuple(discrete_state.astype(np.int))


# main loop for number of iterations run
for episode in range(EPISODES):

    # tracks total reward for a episode
    episode_reward = 0

    # gets initial observation in discrete form
    state = env.reset()
    discrete_state = get_discrete_state(state)

    # set flag to render
    if (episode % SHOW_EVERY) == 0:
        render = True
    else:
        render = False


    # main loop for env
    done = False
    while not done:
        # check if random value is greater than epsilon. as epsilon
        # decays then it will be more likely to take action from
        # Q table over random action
        if np.random.random() > epsilon:
            # get action from Q table 
            action = np.argmax(q_table[discrete_state])
        else:
            # get random action
            action = np.random.randint(0, env.action_space.n)


        # step the env and get new discrete state
        new_state, reward, done, _ = env.step(action)
        new_discrete_state = get_discrete_state(new_state)

        # sum reward information
        episode_reward += reward

        # check to render iteration
        if render:
            env.render()

        # if sim in not done then update q table
        if not done:
            # max possible Q value in next step
            max_future_q = np.max(q_table[new_discrete_state])

            # current Q value
            current_q = q_table[discrete_state + (action,)]

            # new q value given the current and max possible 
            new_q = (1 - LEARNING_RATE) * current_q + LEARNING_RATE * (reward + DISCOUNT * max_future_q)

            # update table
            q_table[discrete_state + (action,)] = new_q

        # if goal was reached then directly update q table with reward
        elif new_state[0] >= env.goal_position:
            #q_table[discrete_state + (action,)] = reward
            q_table[discrete_state + (action,)] = 0

        # update last state
        discrete_state = new_discrete_state


    # decay epsilon value
    if END_EPSILON_DECAYING >= episode >= START_EPSILON_DECAYING:
        epsilon -= EPSILON_DECAY_VALUE


    # storing stats on episodes
    ep_rewards.append(episode_reward)
    if (episode % STATS_EVERY) == 0:
        # gets episode rewards from last time it recorded stats
        # and averages it out
        average_reward = sum(ep_rewards[-STATS_EVERY:])/STATS_EVERY
        # save other stats in array
        aggr_ep_rewards['episode'].append(episode)
        aggr_ep_rewards['avg'].append(average_reward)
        aggr_ep_rewards['max'].append(max(ep_rewards[-STATS_EVERY:]))
        aggr_ep_rewards['min'].append(min(ep_rewards[-STATS_EVERY:]))
        print(f'episode: {episode:>5d}, average reward {average_reward:4.1f}, current epsilon {epsilon:>1.2f}')


    # save Q table
    if (episode % SAVE_EVERY) == 0:
        np.save(f"qtables/{episode}-qtable.npy", q_table)

env.close()

plt.plot(aggr_ep_rewards['episode'], aggr_ep_rewards['avg'], label="average rewards")
plt.plot(aggr_ep_rewards['episode'], aggr_ep_rewards['min'], label="min rewards")
plt.plot(aggr_ep_rewards['episode'], aggr_ep_rewards['max'], label="max rewards")
plt.grid(True)
plt.legend(loc=4)
plt.show()

