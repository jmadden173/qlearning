
import os
import time
import matplotlib.pyplot as plt


import tensorflow as tf
from tensorflow.keras import datasets, layers, models


# CONSTANTS
DEBUG = False
# names of labels
class_names = [
    'airplane',
    'automobile',
    'bird',
    'cat',
    'deer',
    'dog',
    'frog',
    'horse',
    'ship',
    'truck',
]


# SETTINGS
CUR_DIR = os.path.dirname(os.path.abspath(__file__))
LOG_DIR = os.path.join(os.path.join(CUR_DIR,"logs"), str(time.time()))


# load data
(train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()

# normalize rgb values
train_images = train_images / 255
test_images = test_images / 255

if DEBUG:
    # verify data
    plt.figure(figsize=(10,10))
    for i in range(25):
        plt.subplot(5,5,i+1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(train_images[i], cmap=plt.cm.binary)
        # CIFAR labels are arrays so need extra index
        plt.xlabel(class_names[train_labels[i][0]])
    plt.show()

# build model
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.Flatten())
model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(len(class_names)))

print(model.summary())

tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=LOG_DIR)

model.compile(
    optimizer="adam",
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=["accuracy"],
)

history = model.fit(
    train_images,
    train_labels,
    epochs=10,
    validation_data=(test_images, test_labels),
)

test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=2)
print(test_acc)