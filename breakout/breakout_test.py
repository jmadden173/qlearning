import gym

env = gym.make("Breakout-v0")

observation = env.reset()
for _ in range(500):
    env.render()
    action = env.action_space.sample()
    print(action)
    observation, reward, done, info = env.step(action)

    if done:
        observation = env.reset()

env.close()