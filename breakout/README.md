# Breakout

## breakout_test.py

Uses a random agent to test if gym[atari] enviroments are working. On windows a different package is needed that does not required linux dependencies.
See [Kojoley/atari-py](https://github.com/Kojoley/atari-py) for more information.

## breakout_dqn.py

Uses reinforcement learning to test reinforcement learning. Almost a carbon copy of `blob_dqn.py` but with a different enviroment to solidify my knowledge on reinfocement learning.
See `tutorial/blob_dqn.py` for where most of this code came from.