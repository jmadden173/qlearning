import gym

# general
import os
import numpy as np
import pickle
import gym
import time
from tqdm import tqdm
import random
from collections import deque

# keras
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout, Conv2D,  MaxPooling2D, Activation, Flatten
from keras.callbacks import TensorBoard
from keras.optimizers import Adam


# CONSTANTS
DISCOUNT = 0.99
REPLAY_MEMORY_SIZE = 50_000
MIN_REPLAY_MEMORY_SIZE = 1000
MODEL_NAME = "256x2"
MINIBATCH_SIZE = 64
UPDATE_TARGET_EVERY = 5

# env settings
EPISODES = 20_000

# exploration settings
epsilon = 1
EPSILON_DECAY = 0.99975
MIN_EPSILON = 0.001

# stats
AGGREGATE_STATS_EVERY = 50
SHOW_PREVIEW = True


print(tf.__version__)
# disable gpu bc to much memory is being used
tf.config.set_visible_devices([], 'GPU')
#gpus = tf.config.experimental.list_physical_devices('GPU')
#if gpus:
#    try:
#        for gpu in gpus:
#            tf.config.experimental.set_memory_growth(gpu, True)
#        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
#    except RuntimeError as e:
#        print(e)




# make gym enviroment
env  = gym.make("Breakout-v0")
print(env.observation_space.shape)
print(env.action_space.n)


# takes from here for update to tensorflow 2.0
# https://stackoverflow.com/questions/58711624/modifying-tensorboard-in-tensorflow-2-0
class ModifiedTensorBoard(TensorBoard):
    # overriding init to set initial step and writer
    # we want one log file for all .fit() calls
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step = 1
        self.writer = tf.summary.create_file_writer(self.log_dir)
        self._log_write_dir = os.path.join(self.log_dir, MODEL_NAME)


    # overrided, to prevent creation of default log writer
    def set_model(self, model):
        pass

    # overrided, saves logs with step number
    # (normally .fit() will begin writing from the 0th step)
    def on_epoch_end(self, epoch, logs=None):
        self.update_stats(**logs)

    # overrided no need to save anything at epoch end
    def on_batch_end(self, batch, logs=None):
        pass

    # overrided so writer is not closed
    def on_train_end(self, _):
        pass

    # writer for saving custom metrics
    def update_stats(self, **stats):
        self._write_logs(stats, self.step)

    # writes logs to file
    def _write_logs(self, logs, index):
        with self.writer.as_default():
            for name, value in logs.items():
                tf.summary.scalar(name, value, step=index)
                self.step += 1
                self.writer.flush()



class DQNAgent:
    def __init__(self):
        # main model
        self.model = self.create_model()

        # target network
        self.target_model = self.create_model()
        self.target_model.set_weights(self.model.get_weights())

        # replay buffer (numpy array) to store that contains last n steps for
        # training
        self.replay_memory = deque(maxlen=REPLAY_MEMORY_SIZE)

        # custom tensorboard object
        #self.tensorboard = TensorBoard(
        #    log_dir="logs/{}-{}".format(MODEL_NAME, int(time.time()))
        #)
        self.tensorboard = ModifiedTensorBoard(
            log_dir="logs/{}-{}".format(MODEL_NAME, int(time.time()))
        )

        # used to decide when to update target network with
        # main network's weights
        self.target_update_counter = 0


    def create_model(self):
        '''create a convolutional neural network'''

        model = Sequential()

        # input 
        model.add(Conv2D(256, (3,3), input_shape=env.observation_space.shape))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.2))

        model.add(Conv2D(256, (3,3)))
        model.add(Activation("relu"))
        model.add(MaxPooling2D(pool_size=(2,2)))
        model.add(Dropout(0.2))

        model.add(Flatten())
        model.add(Dense(64))

        # output
        model.add(Dense(env.action_space.n, activation="linear"))
        model.compile(loss="mse", optimizer=Adam(lr=0.001), metrics=["accuracy"])

        return model


    def update_replay_memory(self, transition):
        '''adds step data to memory replay array

        :param transition: (observation_space, action, reward, new_observation, done)
        '''

        self.replay_memory.append(transition)


    def get_qs(self, state):
        '''queries main network for Q value for a given state
        
        :param state: the enviroment state
        '''
        return self.model.predict(np.array(state).reshape(-1, *state.shape)/255)[0]


    def train(self, terminal_state, step):
        '''trains the neural network every step during a episode'''

        # start training if above a certain amount of samples
        if len(self.replay_memory) < MIN_REPLAY_MEMORY_SIZE:
            return

        # get a sample from the replay_memory to use in training
        minibatch = random.sample(self.replay_memory, MINIBATCH_SIZE)

        # NOTE i think were dividing by 255 to normalize the rgb
        # values from the env

        # gets the states from the minibatch, then query NN for q values
        current_states = np.array([transition[0] for transition in minibatch])/255
        current_qs_list = self.model.predict(current_states)
        
        # gets the futurestates from the minibatch, then query NN for q values
        # when using target network, query it, otherwise use main network ?????
        new_current_states = np.array([transition[3] for transition in minibatch])/255
        future_qs_list = self.target_model.predict(new_current_states)


        x = []
        y = []

        # enumerate batches
        for index, (current_state, action, reward, new_current_state, done) in enumerate(minibatch):
            # if not in terminal state aka if the env is done, get new Q
            # from future states, otherwise set to zero
            # almost like Q learning but only part of the equation is used
            if not done:
                max_future_q = np.max(future_qs_list[index])
                new_q = reward + DISCOUNT * max_future_q
            else:
                new_q = reward

            # update Q value for a given state
            current_qs = current_qs_list[index]
            current_qs[action] = new_q

            # append to training data
            x.append(current_state)
            y.append(current_qs)

        # fit on all samples as a batch
        # only have tensorboard callback if in terminal state
        self.model.fit(
            np.array(x)/255,
            np.array(y),
            batch_size=MINIBATCH_SIZE,
            verbose=0,
            shuffle=False,
            callbacks=[self.tensorboard] if terminal_state else None
            #callbacks=[self.tensorboard]
        )

        # update the counter every episode
        if terminal_state:
            self.target_update_counter += 1

        # if counter reaches desired values, update weights
        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0



# for stats
ep_rewards = []


agent = DQNAgent()

# loop for running episodes
for episode in tqdm(range(1, EPISODES + 1), ascii=True, unit="episodes"):

    # update tensorboard step to match episode
    #agent.tensorboard.step = episode

    # restart episode
    episode_reward = 0
    step = 1

    # reset env
    current_state = env.reset()

    # loop through until done
    done = False
    while not done:

        if np.random.random_sample() > epsilon:
            # get action from Q table
            action = np.argmax(agent.get_qs(current_state))

        else:
            # TODO change to sample for random action
            # get random state
            action = np.random.randint(0, env.action_space.n)

        # step env
        new_state, reward, done, _ = env.step(action)

        # count reward
        episode_reward += reward

        # display snapshot training process
        if (SHOW_PREVIEW) and (episode % AGGREGATE_STATS_EVERY == 0):
            env.render()

        # update replay buffer and train network
        agent.update_replay_memory((
            current_state,
            action,
            reward,
            new_state,
            done
        ))
        agent.train(done, step)

        # update state
        current_state = new_state
        step += 1


    # logs stats
    ep_rewards.append(episode_reward)
    if (episode % AGGREGATE_STATS_EVERY == 0) or (episode == 1):
        average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])
        min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
        max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
        agent.tensorboard.update_stats(
            reward_avg=average_reward,
            reward_min=min_reward,
            reward_max=max_reward,
            epsilon=epsilon,
        )


    # decay epsilon
    if epsilon > MIN_EPSILON:
        # exponential decay
        epsilon *= EPSILON_DECAY
        epislon = max(MIN_EPSILON, epsilon)